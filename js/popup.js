
$(document).ready( function() {
    $("#action").click( function() {
        chrome.tabs.query(
		    {currentWindow: true, active : true},
		    function(tabArray){
		    	chrome.runtime.sendMessage({
		    		sender: "popup", 
		    		data:{streamitupTabID: tabArray[0].id}
		    	})
		    }
		) 
        
        $("#go-button-text").css("display", "none")
        $("#go-button-proc").css("display", "block")
        $("#action").attr("disabled", "true")
    })
})