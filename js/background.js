// streamitupTabID = null

// videoData = null

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {

    	switch(request.sender) {
    		case "streamitup":
    			if (request.data.videoData) {
    				window.videoData = request.data.videoData
    				
			      	chrome.tabs.create(
		      			{url:"index.template.html"}
	      			)

    			}
		      	break;

	      	case "vidPage":
	      		if (request.data.docReady) {
					chrome.tabs.sendMessage(sender.tab.id,
					{
						sender: "background", 
						data:{videoData: window.videoData}
					})	
	      		}
      		break;

	      	case "popup":
    			if (request.data.streamitupTabID) {
    				streamitupTabID = request.data.streamitupTabID
    				chrome.tabs.executeScript(request.data.streamitupTabID, { file: 'js/lib/jquery-1.11.2.min.js' });
    				chrome.tabs.executeScript(request.data.streamitupTabID, { file: 'js/streamitup.js' }); 
				}
			break;
		}
      	return true
});

