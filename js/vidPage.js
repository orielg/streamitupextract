$(document).ready(function() {
	var videoData = null

	chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
    	if (request.sender == "background" && request.data.videoData) {
    		videoData = request.data.videoData
    		window.videoData = videoData
    		templateData = {videoData: videoData}

			var dataTemplateScript = $("#lesson-data").html();
			var dataTemplate = Handlebars.compile(dataTemplateScript)
			$("#data-main").append(dataTemplate(templateData))
			$("#data-main").accordion({
				header: "h3",
				active: "false",
				collapsible: "true"
			});
			// checkboxes aren't clickable without it
			$('#data-main input[type="checkbox"]').click(function(e) {
    			e.stopPropagation();
			});
		}
	});

	chrome.runtime.sendMessage({
				sender: "vidPage", 
				data:{docReady:true}
	})

	$("#serialize-all").click(function () {
		$(".modal-body").html("")
		var serilizedData = {serializedData: JSON.stringify(videoData)}
		var serializedDataTemplateScript = $("#serialized-data-script").html()
		var serializedDataTemplate = Handlebars.compile(serializedDataTemplateScript)
		$(".modal-body").append(serializedDataTemplate( serilizedData ))
	})

	// $("#serialize-sele").click(function() {
	// 	$(".modal-body").html("")

	// 	checkVids = [];	
	// 	$("input:checked").each(function(idx, cboxEl) {
	// 		videoData.forEach(function(vidEl) {
	// 			if ( $(cboxEl).attr("vid") == vidEl.vid ) {
	// 				checkVids.push(vidEl);
	// 			} 
	// 		})
	// 	})
	// 	console.log(checkVids)	
	// 	var serilizedData = {serializedData: JSON.stringify(checkVids).trim()}
	// 	var serializedDataTemplateScript = $("#serialized-data-script").html()
	// 	var serializedDataTemplate = Handlebars.compile(serializedDataTemplateScript)
	// 	$(".modal-body").append(serializedDataTemplate( serilizedData ))
	// 	// window.open('data:text/csv,' + encodeURIComponent(serilizedData));			
	// })


})
