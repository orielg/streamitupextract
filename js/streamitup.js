var fetchData = function () {
    
    ldata = scrape(document);

    var promises = []
    var lessonsData = []

    // we push each ajax request to an array of promises
    // and process each request individually when done.
    $(ldata).each(function(idx, el) {
        promises.push(
            $.post("/huji_db/database/vod_student_window_E.php",
                {
                    username: el.username,
                    passwd: el.passwd,
                    lesson_url: el.lesson_url
                }
            ).done(function(response) {
                lessonsData[idx] = {
                  name: el.lesson_id,
                  date: el.lesson_date,
                  parts:  
                    $.parseJSON(
                        /var chapters_data = (.*).*;.*/
                        .exec(response)[1]
                    ).map( function(part, idx) {
                        return {
                            full_cam_path: part.full_cam_path,
                            full_scr_path: part.full_scr_path,
                            full_other_path: part.full_other_path,
                            cam_file_type: part.cam_file_type,
                            scr_file_type: part.screen_file_type,
                            other_file_type: part.other_file_type
                        }
                    })
                } 
            })
    )});

    // when all requests are done, we inform the background script
    $.when.apply($, promises).done(function() {
        chrome.runtime.sendMessage({
            sender:"streamitup", 
            data: {videoData: lessonsData}
        });
    }).fail(function() {
        console.error("AJAX FAIL")
    })
}

var getDates = function(input) {
    return $(input).find("table tbody tr td font")
           .filter( function(idx, el) {
                return /(\d{2})\/(\d{2})\/(\d{4})/
                       .exec($(el).text()) ? true : false 
            }).map(function(idx, el) {
                return $(el).text()
            })
}


var scrape = function(input) {
    var lessons = []
        $(['username', 'passwd', 'lesson_url', 'lesson_id'])
        .each(function(tidx, type) {
            $(input).find("input[type='hidden'][name='" + type + "']")
            .map( function(aidx, el) {
                if ( !$.isPlainObject(lessons[aidx]) ) 
                    lessons[aidx] = {}
                lessons[aidx][$(el).attr("name")] = $(el).attr("value")     
            })
        })

        lessons =  $(lessons).filter(function(idx, el) {
                            return (el.lesson_url && el.lesson_id);
                        })
        var dates = getDates(input);
        if (dates.length == lessons.length) {
            $(lessons).each(function(idx, el) {
                lessons[idx].lesson_date = dates[idx];
            })
        }

        return lessons; 
}

fetchData();


