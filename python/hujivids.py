"""
Usage:
python hujivids.py <input file>

The input file should contain a JSON string obtained from StreamItUP Extract chrome extension.

This script uses ffmpeg: https://www.ffmpeg.org/
"""

import sys
import json
import re
from subprocess import call

FFMPEG_BIN = "ffmpeg"
MEGACMD_BIN = "./megacmd"
BLANK_VIDEO_PATH = "mms://194.90.168.134/liorf/siu/blankvideo/blankvideo.wmv"

if len(sys.argv) < 2:
    print "Please provide a data file. Existing..."
    exit()


jsonFile = open(sys.argv[1], 'r')
data = json.loads(jsonFile.read())
jsonFile.close()

for lesson in data:
    j=1
    for part in lesson[u'parts']:
        mms_re = re.compile('^mms:\/\/')
        if not part[u'full_cam_path'] == BLANK_VIDEO_PATH:
            vidName = re.sub("[/\\\]+",
                                 "-",
                                 re.sub("[ ]+" ,"_", lesson[u'name'].encode('utf8')))
            camFileName = "%s_p%s.%s" % ( vidName, j, part[u'cam_file_type'].encode('utf8') )
            camAddr = mms_re.sub("mmsh://", part[u'full_cam_path']) # ffmpeg doesn't recognize "mms://" for some reason
            retVal = call([
                FFMPEG_BIN,
                "-i", camAddr,
                "-c", "copy", camFileName
            ])

            # This uploads the dumped streams to MEGA using megacmd (google it)
            #if retVal == 0:
            #    call([MEGACMD_BIN, "put", camFileName, "mega:/"])

        j = j+1

